﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MyWpfProject
{
    [ValueConversion(typeof(int), typeof(string))]
    public class YearConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value + " г.");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string val = (string)value;
            int res;
            int.TryParse(val.Remove(val.Length - 3, 3), out res);
            return res;
        }
    }
}
