﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using MyWpfProject.Commands;
using MyWpfProject.Models;

namespace MyWpfProject.ViewModels
{
    public class BookViewModel : ViewModelBase
    {
        private Book _book;
        public Window View;
        public ObservableCollection<BookViewModel> Books;
        public int Index;

        public string Name
        {
            get { return _book.Name; }
            set { _book.Name = value; OnPropertyChanged("Name"); }
        }

        public string Author
        {
            get { return _book.Author; }
            set { _book.Author = value; OnPropertyChanged("Author"); }
        }

        public State State
        {
            get { return _book.State; }
            set { _book.State = value; OnPropertyChanged("State"); }
        }

        public Genre Genre
        {
            get { return _book.Genre; }
            set { _book.Genre = value; OnPropertyChanged("Genre"); }
        }

        public int Year
        {
            get { return _book.Year; }
            set { _book.Year = value; OnPropertyChanged("Year"); }
        }

        public BookViewModel(Book book)
        {
            _book = book;
        }

        public BookViewModel(Book book, Window window, ObservableCollection<BookViewModel> m)
        {
            _book = book;
            View = window;
            Books = m;
        }

        public BookViewModel(Book book, Window window, ObservableCollection<BookViewModel> m, int index)
        {
            _book = book;
            View = window;
            Books = m;
            Index = index;
        }

        public void Update(BookViewModel newBook)
        {
            _book = new Book(newBook.Name, newBook.Author, newBook.Genre, newBook.Year);
        }

        private bool FuncToEvaluate(object context)
        {
            return true;
        }

        public ICommand EditCommand => new DelegateCommand<BookViewModel>(EditBook, FuncToEvaluate);

        private void EditBook(object context)
        {
            BookViewModel newBook = (BookViewModel)View.DataContext;
            if (Index != -1)
            {
                Books[Index] = newBook;
                Books[Index].OnPropertyChanged("Year");
            }
            View.Close();
        }
    }
}
