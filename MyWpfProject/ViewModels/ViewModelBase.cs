﻿using System.ComponentModel;

namespace MyWpfProject.ViewModels
{
    // Предоставляет основной функционал ViewModel-классам
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public ViewModelBase SelectedSong { get; set; }

        // событие при изменении компонента модели
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
