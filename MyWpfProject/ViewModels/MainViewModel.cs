﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using MyWpfProject.Commands;
using MyWpfProject.Models;
using MyWpfProject.Views;

namespace MyWpfProject.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public ObservableCollection<BookViewModel> BooksList { get; set; }

        public MainViewModel(IEnumerable<Book> books)
        {
            BooksList = new ObservableCollection<BookViewModel>(books.Select(book => new BookViewModel(book)));
            EventManager.RegisterClassHandler(typeof(ListBoxItem), UIElement.MouseLeftButtonDownEvent,
                new RoutedEventHandler(EditAction));
        }

        private bool _groupView;
        public bool GroupView
        {
            get { return _groupView; }
            set
            {
                if (value == _groupView) return;
                var view = CollectionViewSource.GetDefaultView(BooksList);
                view.GroupDescriptions.Clear();
                if (value)
                    view.GroupDescriptions.Add(new PropertyGroupDescription("Genre"));

                _groupView = value;
                OnPropertyChanged("GroupView");
            }
        }

        private bool _filterDisabled;
        public bool FilterDisabled
        {
            get { return _filterDisabled; }
            set
            {
                if (value == _filterDisabled) return;
                var view = CollectionViewSource.GetDefaultView(BooksList);

                if (value)
                {
                    foreach (var book in BooksList)
                        book.State = State.None;
                    view.Refresh();
                }
                _filterDisabled = value;
                OnPropertyChanged("FilterDisabled");
            }
        }

        public void UpdateBook(BookViewModel newBook)
        {
            BooksList.FirstOrDefault(bookViewModel => bookViewModel.Name == newBook.Name)?.Update(newBook);
        }

        public ICommand FilterCommand => new RelayCommand(Filter);

        private void Filter(object context)
        {
            FilterDisabled = false;
            var values = (object[])context;
            if (((string)values[0]).Length == 0 && ((string)values[1]).Length == 0)
            {
                foreach (var book in BooksList)
                    book.State = State.None;
                return;
            }

            int firstYear, secondYear;
            if (!int.TryParse((string)values[0], out firstYear) || !int.TryParse(values[1] as string, out secondYear))
                return;

            foreach (var book in BooksList)
                if (book.Year >= firstYear && book.Year <= secondYear)
                    book.State = State.Between;
                else
                    if (book.Year < firstYear)
                    book.State = State.Less;
                else
                        if (book.Year > secondYear)
                    book.State = State.More;
        }

        private void EditAction(object sender, RoutedEventArgs e)
        {
            ListBoxItem item = sender as ListBoxItem;
            BookViewModel bvm = item?.DataContext as BookViewModel;
            if (bvm != null)
            {
                EditBookWindow window = new EditBookWindow();
                int index = BooksList.IndexOf(bvm);
                bvm.View = window;
                bvm.Books = BooksList;
                bvm.Index = index;
                window.DataContext = bvm;
                window.ShowDialog();
            }
        }
    }
}
