﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using MyWpfProject.Models;
using MyWpfProject.ViewModels;

namespace MyWpfProject
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        public List<Book> Books;
        void AppStartup(object sender, StartupEventArgs args)
        {
            Book[] tmp =
            {
                new Book("Идиот", "Федор Достоевский", Genre.Classic, 1868),
                new Book("Алиса в стране чудес", "Льюис Кэролл", Genre.Fantasy, 1865),
                new Book("Оно", "Стивен Кинг", Genre.Horror, 1986),
                new Book("Ромео и Джульетта", "Вильям Шекспир", Genre.Tragedy, 1595),
                new Book("Безнадега", "Стивен Кинг", Genre.Horror, 1966)
            };
            Books = tmp.ToList();
            MainWindow mainWindow = new MainWindow();
            MainViewModel mainViewModel = new MainViewModel(Books);
            mainWindow.DataContext = mainViewModel;
            mainWindow.Show();
        }
    }
}
