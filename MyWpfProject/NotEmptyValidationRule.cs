﻿using System.Globalization;
using System.Windows.Controls;

namespace MyWpfProject
{
    public class NotEmptyValidationRule : System.Windows.Controls.ValidationRule
    {
        static readonly string ErrorMessage = "You should fill this field!";
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (string.IsNullOrEmpty((string)value))
                return new ValidationResult(false, ErrorMessage);
            //return ValidationResult.ValidResult;
            return new ValidationResult(true, null);
        }
    }
}
