﻿namespace MyWpfProject.Models
{
    public enum Genre
    {
        Classic,
        Fantasy,
        Horror,
        Tragedy
    }

    public enum State
    {
        None,
        Less,
        More,
        Between
    }

    public class Book
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public Genre Genre { get; set; }
        public int Year { get; set; }
        public State State { get; set; }

        public Book() { }

        public Book(string name, string author, Genre genre, int year)
        {
            Name = name;
            Author = author;
            Genre = genre;
            Year = year;
        }
    }
}
