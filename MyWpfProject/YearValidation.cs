﻿using System.Globalization;
using System.Windows.Controls;

namespace MyWpfProject
{
    class YearValidation : ValidationRule
    {
        static readonly string ErrorNegativeMessage = "Year can't be a negative";
        static readonly string ErrorGeneralMessage = "Year is incorrect!";
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int num;
            if (!int.TryParse((string)value, out num))
                return new ValidationResult(false, ErrorGeneralMessage);
            if (num < 0)
                return new ValidationResult(false, ErrorNegativeMessage);
            if (num < 1500 || num > 2016)
                return new ValidationResult(false, ErrorGeneralMessage);
            return ValidationResult.ValidResult;
        }
    }
}
